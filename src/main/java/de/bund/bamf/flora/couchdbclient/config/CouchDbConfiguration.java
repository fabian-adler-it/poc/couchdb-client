package de.bund.bamf.flora.couchdbclient.config;

import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.ssl.SSLContextBuilder;
import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbInstance;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.net.ssl.SSLContext;

@Configuration
public class CouchDbConfiguration {

    @Bean
    public SSLSocketFactory sslSocketFactory(final CouchDbProperties couchDbProperties) throws Exception {
        final SSLContext sslContext = new SSLContextBuilder()
                .loadTrustMaterial(
                        couchDbProperties.getSslHttpClient().getTruststore().getURL(),
                        couchDbProperties.getSslHttpClient().getTruststorePassword().toCharArray())
                .build();
        return new SSLSocketFactory(sslContext);
    }

    @Bean(destroyMethod = "shutdown")
    public HttpClient httpClient(final SSLSocketFactory sslSocketFactory) {
        return new StdHttpClient.Builder()
                .enableSSL(true)
                .sslSocketFactory(sslSocketFactory)
                .host("localhost")
                .port(6984)
                .username("admin")
                .password("password")
                .build();
    }

    @Bean
    public CouchDbInstance couchDbInstance(final HttpClient httpClient) {
        return new StdCouchDbInstance(httpClient);
    }

}
