package de.bund.bamf.flora.couchdbclient;

import de.bund.bamf.flora.couchdbclient.config.CouchDbProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({ CouchDbProperties.class })
public class CouchDbClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(CouchDbClientApplication.class, args);
	}

}
