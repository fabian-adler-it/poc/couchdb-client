package de.bund.bamf.flora.couchdbclient.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.core.io.Resource;

@ConfigurationProperties(prefix = "couchdb")
@ConstructorBinding
@AllArgsConstructor
@Getter
public class CouchDbProperties {

    private SslHttpClient sslHttpClient;

    @ConstructorBinding
    @AllArgsConstructor
    @Getter
    public static class SslHttpClient {

        private Resource truststore;
        private String truststorePassword;

    }

}
