package de.bund.bamf.flora.couchdbclient.service;

import lombok.RequiredArgsConstructor;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.impl.StdCouchDbConnector;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CouchDbService {

    private final CouchDbInstance couchDbInstance;

    public List<String> getAllElemetIDsFromDatabase(final String databaseName) {
        return connectToDatabase(databaseName).getAllDocIds();
    }

    private CouchDbConnector connectToDatabase(final String databaseName) {
        if (!couchDbInstance.checkIfDbExists(databaseName)) {
            throw new IllegalArgumentException("Database with name '" + databaseName + "' not found");
        }
        return new StdCouchDbConnector(databaseName, couchDbInstance);
    }

}
