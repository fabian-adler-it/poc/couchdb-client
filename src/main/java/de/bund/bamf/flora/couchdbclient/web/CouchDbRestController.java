package de.bund.bamf.flora.couchdbclient.web;

import de.bund.bamf.flora.couchdbclient.service.CouchDbService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("couch")
@RequiredArgsConstructor
public class CouchDbRestController {

    private final CouchDbService couchDbService;

    @GetMapping
    public ResponseEntity<List<String>> getAllElementIDs(@RequestParam final String db) {
        try {
            return ResponseEntity.ok(couchDbService.getAllElemetIDsFromDatabase(db));
        } catch (final IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }
    }

}
